export const Requests = new Mongo.Collection('requests');

const RequestsSchema = new SimpleSchema({
  userId: {
    type: String,
  },
  startDate: {
    type: Date
  },
  endDate: {
    type: Date
  },
  status: {
    type: String,
    autoValue: function() {
      return this.isInsert && !this.isSet ? 'pending' : undefined;
    }
  },
  team: {
    type: String
  },
  createdBy: {
    type: String,
    label: 'CreatedBy',
    autoValue: function () {
      if (this.isInsert && !this.isSet) {
        return this.userId ? this.userId : 'system';
      } else {
        return undefined;
      };
    }
  },
  createdAt: {
    type: Date,
    label: 'CreatedAt',
    autoValue: function () {
      if (this.isInsert && !this.isSet) {
        return new Date();
      } else {
        return undefined;
      };
    }
  },
  updatedAt: {
    type: Date,
    autoValue: function () {
      return new Date();
    }
  },
  updatedBy: {
    type: String,
    autoValue: function () {
      if (this.userId) {
        return this.userId;
      }
      return 'system';
    }
  }
});

Requests.attachSchema(RequestsSchema);
