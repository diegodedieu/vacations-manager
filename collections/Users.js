const Schema = {};

Schema.UserProfile = new SimpleSchema({
  firstName: {
    type: String,
    optional: true
  },
  lastName: {
    type: String,
    optional: true
  },
  phone: {
    type: String,
    optional: true
  },
  image: {
    type: String,
    optional: true
  }
});

Schema.User = new SimpleSchema({
  username: {
    type: String,
    optional: true
  },
  emails: {
    type: Array,
    optional: false
  },
  "emails.$": {
    type: Object
  },
  "emails.$.address": {
    type: String,
    regEx: SimpleSchema.RegEx.Email
  },
  "emails.$.verified": {
    type: Boolean,
    autoValue: function () {
      if (!this.isSet) {
        return false;
      };
    }
  },
  createdAt: {
    type: Date,
    autoValue: function () {
      if (this.isInsert && !this.isSet) {
        return new Date();
      } else {
        return undefined;
      };
    }
  },
  profile: {
    type: Schema.UserProfile,
    optional: true
  },
  services: {
    type: Object,
    optional: true,
    blackbox: true,
  },
  roles: {
    type: Object,
    blackbox: true,
    optional: true
  },
});

Meteor.users.attachSchema(Schema.User);

Meteor.users.allow({
  update: function (userId, doc) {
    return !!userId;
  }
});
