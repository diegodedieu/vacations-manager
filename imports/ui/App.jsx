import React from 'react';
import { Router } from '@reach/router';
import { createTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from '../stores/store';
import Hello from './Hello';
import LoginPage from './Login/LoginPage';
import RegisterPage from './Register/RegisterPage';
import DashboardPage from './Dashboard/DashboardPage';
import EmployeePage from './Examples/EmployeePage';
import EmployerPage from './Examples/EmployerPage';
import '../styles/global.scss';

const theme = createTheme({
  palette: {
    primary: {
      main: '#109CF1',
      contrastText: '#FFFFFF',
    },
  },
});

const App = () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <ThemeProvider theme={theme}>
        <Router>
          <Hello path="/" />
          <LoginPage path="/login" />
          <RegisterPage path="/register" />
          <DashboardPage path="/dashboard" />
          <EmployeePage path="/employee" />
          <EmployerPage path="/employer" />
        </Router>
      </ThemeProvider>
    </PersistGate>
  </Provider>
);

export default App;
