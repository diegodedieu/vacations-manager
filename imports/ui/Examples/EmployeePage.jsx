import React, { useState, useEffect } from 'react';
import { Button, Grid, TextField } from '@material-ui/core';
import { Meteor } from 'meteor/meteor';
import { useTracker } from 'meteor/react-meteor-data';

const EmployeePage = () => {
	const [ start, setStart ] = useState(moment().format('YYYY-MM-DD'));
	const [ end, setEnd ] = useState(moment().format('YYYY-MM-DD'));

	const user = useTracker(() => Meteor.userId());

	useEffect(() => {
    if (!user) navigate('/');
  }, [user]);

	const sendRequest = () => {
		const startDate = new Date(start);
		const endDate = new Date(end);
		if (startDate < new Date()) return alert('You cannot request for past dates');
		if (endDate < startDate) return alert('End date must be later than start date');
		Meteor.call('requests.insert', { startDate, endDate, team: 'test' }, (err) => {
			if (err) {
				console.log(err);
				alert(err.reason);
			} else alert('Request sent!');
		});
	}

	return (
		<Grid 
      container
      direction="column"
      justifyContent="center"
      alignItems="center"
      spacing={3}
    >
    	<Grid
    		item
    		xs={12}
  		>
  			<TextField
  				type="date"
          label="Start date"
          value={start}
          onChange={(e) => setStart(e.target.value)}
  			/>
  		</Grid>
  		<Grid
    		item
    		xs={12}
  		>
  			<TextField
  				type="date"
          label="Start date"
          value={end}
          onChange={(e) => setEnd(e.target.value)}
  			/>
  		</Grid>
    	<Grid
    		item
    		xs={12}
  		>
  			<Button
  				variant="contained"
          color="primary"
          size="large"
          onClick={sendRequest}
        >
        	Send
        </Button>
  		</Grid>
    </Grid>
	)
}

export default EmployeePage;