import React, { useState, useEffect } from 'react';
import { Button, Grid, TextField } from '@material-ui/core';
import { Meteor } from 'meteor/meteor';
import { useTracker } from 'meteor/react-meteor-data';
import { Requests } from '../../../collections/Requests';

const EmployerPage = () => {
  const user = useTracker(() => Meteor.userId());

  useEffect(() => {
    if (!user) navigate('/');
  }, [user]);

  const subs = useTracker(() => {
    const handler = [Meteor.subscribe('requests', 'test')];

    if (handler.some(h => !h.ready())) {
      return {
        loading: true,
        requests: false,
        approved: false,
        declined: false
      };
    };

    return { 
      loading: false,
      pending: Requests.find({ status: 'pending' }).fetch(),
      approved: Requests.find({ status: 'approved' }).fetch(),
      declined: Requests.find({ status: 'declined' }).fetch()
    }
  });

  const { pending, approved, declined } = subs;

  const getName = (id) => {
    const user = Meteor.users.findOne(id);
    return user.profile?.firstName ? `${user.profile.firstName} ${user.profile.lastName}` : user.emails[0].address;
  }

  const answerRequest = (id, approved) => {
    const status = approved ? 'approved' : 'declined';
    Meteor.call('requests.update', id, { status }, (err) => {
      if (err) {
        console.log(err);
        alert(err.reason);
      };
    });
  }

  return (
    <Grid 
      container
      direction="Row"
      justifyContent="center"
      alignItems="center"
      spacing={3}
    >
      <Grid
        item
        xs={4}
      >
        <h1>Pending requests</h1>
        {pending && pending.map((request) => (
          <div key={request._id}>
            <Grid
              item
              xs={12}
            >
              <h3>{getName(request.userId)}</h3>
              <p>{moment(request.startDate).format('DD/MM/YYYY')} - {moment(request.endDate).format('DD/MM/YYYY')}</p>
            </Grid>
            <Grid
              item
              xs={12}
            >
              <Button
                variant="contained"
                color="primary"
                size="large"
                onClick={() => answerRequest(request._id, true)}
              >
                Approve
              </Button>
              <Button
                variant="contained"
                color="secondary"
                size="large"
                onClick={() => answerRequest(request._id, false)}
              >
                Decline
              </Button>
            </Grid>
          </div>
        ))}
      </Grid>

      <Grid
        item
        xs={4}
      >
        <h1>Approved requests</h1>
        {approved && approved.map((request) => (
          <div key={request._id}>
            <Grid
              item
              xs={12}
            >
              <h3>{getName(request.userId)}</h3>
              <p>{moment(request.startDate).format('DD/MM/YYYY')} - {moment(request.endDate).format('DD/MM/YYYY')}</p>
            </Grid>
          </div>
        ))}
      </Grid>
      <Grid
        item
        xs={4}
      >
        <h1>Declined requests</h1>
        {declined && declined.map((request) => (
          <div key={request._id}>
            <Grid
              item
              xs={12}
            >
              <h3>{getName(request.userId)}</h3>
              <p>{moment(request.startDate).format('DD/MM/YYYY')} - {moment(request.endDate).format('DD/MM/YYYY')}</p>
            </Grid>
          </div>
        ))}
      </Grid>
    </Grid>
  )
}

export default EmployerPage;