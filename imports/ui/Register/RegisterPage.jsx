import React, {useState, useEffect} from 'react';
import {Meteor} from 'meteor/meteor';
import {useTracker} from 'meteor/react-meteor-data';
import {Button, Grid, TextField} from '@material-ui/core';
import {Link, navigate} from '@reach/router';
import '../../styles/ui/Login.scss';

const RegisterPage = () => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const user = useTracker(() => Meteor.user());

  useEffect(() => {
    if (user) navigate('/dashboard');
  }, [user]);

  const handleRegister = () => {
    if (!email || !firstName || !lastName || !phone || !password) return alert('You must fill all fields');
    if (password !== confirmPassword) return alert("Passwords doesn't match");
    Accounts.createUser({
      email,
      password,
      profile: {
        firstName,
        lastName,
        phone
      }
    }, (err) => {
      if (err) alert(err.reason);
      else navigate('/dashboard');
    });
  };

  const handleKeyUp = (e) => {
    if (e.key === 'Enter') handleRegister();
  }

  return (
    <>
      <Grid 
        container
        direction="column"
        justifyContent="center"
        alignItems="center"
        spacing={3}
      >
        <h1>Register</h1>
        <Grid
         container
         direction="row"
         className="registerForm"
         spacing={4}
        >
          <Grid item xs={6}>
            <TextField
              className="w100"
              type="text"
              label="First name"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
              onKeyUp={(e) => handleKeyUp(e)}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              className="w100"
              type="text"
              label="Last name"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
              onKeyUp={(e) => handleKeyUp(e)}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              className="w100"
              type="email"
              label="Email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              onKeyUp={(e) => handleKeyUp(e)}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              className="w100"
              type="text"
              label="Phone"
              value={phone}
              onChange={(e) => setPhone(e.target.value)}
              onKeyUp={(e) => handleKeyUp(e)}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              className="w100"
              type="password"
              label="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              onKeyUp={(e) => handleKeyUp(e)}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              className="w100"
              type="password"
              label="Confirm password"
              value={confirmPassword}
              onChange={(e) => setConfirmPassword(e.target.value)}
              onKeyUp={(e) => handleKeyUp(e)}
            />
          </Grid>
          <Grid item xs={12}>
            <Button
              variant="contained"
              color="primary"
              size="large"
              className="w100"
              onClick={handleRegister}
            >
              Sign up
            </Button>
          </Grid>
        </Grid>
        <p>
          <Link to="/forgot-password">Forgot your password?</Link>
        </p>
        <p>
          Already have an account? <Link to="/login">Login</Link>
        </p>
      </Grid>
    </>
  );
};

export default RegisterPage;
