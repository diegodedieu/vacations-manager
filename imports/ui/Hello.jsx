import React, { useState } from 'react';
import {Link} from '@reach/router';

const Hello = () => {
  return (
    <div>
      <Link to="/login">LOGIN</Link>
      <p>or</p>
      <Link to="/register">REGISTER</Link>
    </div>
  );
};

export default Hello;
