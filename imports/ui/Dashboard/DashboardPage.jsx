import React, { useState, useEffect } from 'react';
import { Meteor } from 'meteor/meteor';
import { useTracker } from 'meteor/react-meteor-data';
import { Button, Grid } from '@material-ui/core';
import { navigate } from '@reach/router';

const DashboardPage = () => {
	const user = useTracker(() => Meteor.userId());

	useEffect(() => {
    if (!user) navigate('/');
  }, [user]);

	return (
		<Grid 
			container
			justifyContent="center"
			alignItems="center"
			spacing={3}
		>
			<Grid
				item
				xs={6}
			>
				<Button
					variant="contained"
          color="primary"
          size="large"
          onClick={() => navigate('/employee')}
				>
					Employee
				</Button>
			</Grid>
			<Grid
				item
				xs={6}
			>
				<Button
					variant="contained"
          color="primary"
          size="large"
          onClick={() => navigate('/employer')}
				>
					Employer
				</Button>
			</Grid>
		</Grid>
	)
}

export default DashboardPage;