import React, {useState, useEffect} from 'react';
import {Meteor} from 'meteor/meteor';
import {useTracker} from 'meteor/react-meteor-data';
import {Button, Grid, TextField} from '@material-ui/core';
import {Link, navigate} from '@reach/router';
import '../../styles/ui/Login.scss';

const LoginPage = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const user = useTracker(() => Meteor.user());

  useEffect(() => {
    if (user) navigate('/dashboard');
  }, [user]);

  const handleLogin = () => {
    if (!email || !password) return alert('You must fill all fields');

    Meteor.loginWithPassword(email, password, function(err) {
      if (err) {
        console.log(err);
        alert(err.reason);
      };
    });
  };

  const handleKeyUp = (e) => {
    if (e.key === 'Enter') handleLogin();
  }

  return (
    <>
      <Grid 
        container
        direction="column"
        justifyContent="center"
        alignItems="center"
        spacing={3}
      >
        <h1>Log In</h1>
        <Grid item xs={12}>
          <TextField
            type="text"
            label="Email or username"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            onKeyUp={(e) => handleKeyUp(e)}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            type="password"
            label="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            onKeyUp={(e) => handleKeyUp(e)}
          />
        </Grid>
        <Grid item xs={12}>
          <Button
            variant="contained"
            color="primary"
            size="large"
            onClick={handleLogin}
          >
            Log In
          </Button>
        </Grid>
        <p>
          <Link to="/forgot-password">Forgot your password?</Link>
        </p>
        <p>
          Don’t have an account? <Link to="/register">Please register</Link>
        </p>
      </Grid>
    </>
  );
};

export default LoginPage;
