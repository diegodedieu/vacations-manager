const initialState = {
  count: 0
};

export default function (state = initialState, action) {
  switch (action.type) {
    case 'ADD_COUNT':
      return {
        ...state,
        count: state.count + 1,
      };
    case 'RESET_COUNT':
      return {
        ...state,
        count: 0,
      };
    case 'TOOGLE_MENU':
      return {
        ...state,
        menu: !state.menu,
      };
    default:
      return state;
  }
}
