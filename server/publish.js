import { Meteor } from 'meteor/meteor';
import { Requests } from '../collections/Requests';

Meteor.publish(null, (() => {
  if (this.userId) {
    return Meteor.roleAssignment.find({ 'user._id': this.userId });
  } else {
    return Meteor.roleAssignment.find({ _id: null });
  }
}));

Meteor.publish('requests', function(team) {
  const requests = Requests.find({ team });
  const userIds = requests.map((r) => r.userId);
  return [Requests.find({ team }), Meteor.users.find({ _id: { $in: userIds } })];
})

Meteor.publish('pendingRequests', function(team) {
  const requests = Requests.find({ team, status: 'pending' });
  const userIds = requests.map((r) => r.userId);
  return [Requests.find({ team, status: 'pending' }), Meteor.users.find({ _id: { $in: userIds } })];
})
