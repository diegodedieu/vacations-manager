import { Meteor } from 'meteor/meteor';
import { Requests } from '../../collections/Requests';

if (Meteor.isServer) {
  Meteor.methods({
    'requests.insert'(obj) {
  		obj.userId = this.userId;
  		return Requests.insert(obj);
    },
    'requests.update'(id, obj) {
    	return Requests.update(id, { $set: obj });
    }
  });
};